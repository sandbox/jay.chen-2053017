<?php

/**
 * Implements hook_field_info().
 */
function cycle2_field_info() {
  return array(
    'cycle2' => array(
      'label' => t('Cycle2'),
      'description' => t(''),
      'default_widget' => 'cycle2_default',
      'default_formatter' => 'cycle2_default',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function cycle2_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {

}

/**
 * Implements hook_field_is_empty().
 */
function cycle2_field_is_empty($item, $field) {
  return empty($item['options']);
}


/**
 * Implements hook_field_widget_info().
 */
function cycle2_field_widget_info() {
  return array(
    'cycle2_default' => array(
      'label' => t('Default'),
      'field types' => array('cycle2'),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function cycle2_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['options']) ? $items[$delta]['options'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {

    case 'cycle2_default':
      $options['cycle2']['fx'] = array(
				'#title' => t('Fx'),
        '#type' => 'select',
        '#default_value' => t('fade'),
				'#options'        => array(
					'none'      => t('None'),
					'blindX'      => t('blindX'),
					'blindY'      => t('blindY'),
					'blindZ'      => t('blindZ'),
					'cover'       => t('cover'),
					'curtainX'    => t('curtainX'),
					'curtainY'    => t('curtainY'),
					'fade'        => t('fade'),
					'fadeZoom'    => t('fadeZoom'),
					'growX'       => t('growX'),
					'growY'       => t('growY'),
					'scrollUp'    => t('scrollUp'),
					'scrollDown'  => t('scrollDown'),
					'scrollLeft'  => t('scrollLeft'),
					'scrollRight' => t('scrollRight'),
					'scrollHorz'  => t('scrollHorz'),
					'scrollVert'  => t('scrollVert'),
					'shuffle'     => t('shuffle'),
					'slideX'      => t('slideX'),
					'slideY'      => t('slideY'),
					'toss'        => t('toss'),
					'turnUp'      => t('turnUp'),
					'turnDown'    => t('turnDown'),
					'turnLeft'    => t('turnLeft'),
					'turnRight'   => t('turnRight'),
					'uncover'     => t('uncover'),
					'wipe'        => t('wipe'),
					'zoom'        => t('zoom'),
				),
      );
      $options['cycle2']['easing'] = array(
				'#title' => t('Easing'),
        '#type' => 'select',
        '#default_value' => 'null',
				'#options' => array(
					'null' => t('None'),
				),
      );
      $options['cycle2']['timeout'] = array(
				'#title' => t('Timeout'),
        '#type' => 'textfield',
        '#default_value' => '2000',
        '#size' => 5,
      );
      $options['cycle2']['speed'] = array(
				'#title' => t('Speed'),
        '#type' => 'textfield',
        '#default_value' => '2000',
        '#size' => 5,
      );
			$options['cycle2']['pause-on-hover'] = array(
				'#title' => t('Pause on hover'),
        '#type' => 'select',
        '#default_value' => '1',
        '#options' => array(
					'1' => t('Yes'), 
					'0' => t('No'),
				),
      );
			
			$options['slideshow']['show_overlay'] = array(
				'#title' => t('Show overlay'),
        '#type' => 'select',
        '#default_value' => '1',
        '#options' => array(
					'1' => t('Yes'),
					'0' => t('No'),
				),
      );
			$options['slideshow']['show_pager'] = array(
				'#title' => t('Show pager'),
        '#type' => 'select',
        '#default_value' => '1',
        '#options' => array(
					'1' => t('Yes'),
					'0' => t('No'),
				),
      );
			$options['slideshow']['show_caption'] = array(
				'#title' => t('Show caption'),
        '#type' => 'select',
        '#default_value' => '1',
        '#options' => array(
					'1' => t('Yes'),
					'0' => t('No'),
				),
      );
			
      break;
			
	}

  $element['options'] = $options;
	$element['options']['#type'] = 'fieldset';
	$element['options']['#collapsible'] = TRUE;
	$element['options']['#collapsed'] = false;
	$element['options']['#tree'] = TRUE;
  return $element;
}


/**
 * Implements hook_field_formatter_info().
 */
function cycle2_field_formatter_info() {
  $formatters = array(
    'cycle2_default' => array(
      'label'       => t('Cycle2 - Basic'),
      'field types' => array('field_collection'),
      'settings'    => array(
        'slideshow_image_style'               => '',
        'slideshow_link'                      => '',
        'slideshow_colorbox_image_style'      => '',
        'slideshow_colorbox_slideshow'        => '',
        'slideshow_colorbox_slideshow_speed'  => '4000',
        'slideshow_colorbox_transition'       => 'elastic',
        'slideshow_colorbox_speed'            => '350',
        'slideshow_caption'                   => '',
        'slideshow_caption_link'              => '',
        'slideshow_fx'                        => 'fade',
        'slideshow_speed'                     => '1000',
        'slideshow_timeout'                   => '4000',
        'slideshow_order'                     => '',
        'slideshow_controls'                  => 0,
        'slideshow_controls_pause'            => 0,
        'slideshow_controls_position'         => 'after',
        'slideshow_pause'                     => 0,
        'slideshow_start_on_hover'            => 0,
        'slideshow_pager'                     => '',
        'slideshow_pager_position'            => 'after',
        'slideshow_pager_image_style'         => '',
        'slideshow_carousel_image_style'      => '',
        'slideshow_carousel_visible'          => '3',
        'slideshow_carousel_scroll'           => '1',
        'slideshow_carousel_speed'            => '500',
        'slideshow_carousel_vertical'         => 0,
        'slideshow_carousel_circular'         => 0,
        'slideshow_carousel_follow'           => 0,
        'slideshow_field_collection_image'    => '',
        'slideshow_carousel_skin'             => '',
      ),
    ),
  );
  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function cycle2_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE);
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  $link_types = array(
    'content' => t('Content'),
    'file' => t('File'),
  );
  $element['image_link'] = array(
    '#title' => t('Link image to'),
    '#type' => 'select',
    '#default_value' => $settings['image_link'],
    '#empty_option' => t('Nothing'),
    '#options' => $link_types,
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function cycle2_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $image_styles = image_style_options(FALSE);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (isset($image_styles[$settings['slideshow_image_style']])) {
    $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['slideshow_image_style']]));
  }
  else $summary[] = t('Original image');

  $link_types = array(
    'content'   => t('content'),
    'file'      => t('file'),
    'colorbox'  => t('Colorbox'),
  );
  if ($field['type'] == 'media' || $field['type'] == 'field_collection')
    $link_types += _field_slideshow_get_fields(array('link_field', 'node_reference'), $field['type'], $field['field_name']);
  // Display this setting only if image is linked.
  if (isset($link_types[$settings['slideshow_link']])) {
    $link_type_message = t('Link to: @link', array('@link' => $link_types[$settings['slideshow_link']]));
    if ($settings['slideshow_link'] == 'colorbox') {
      $link_type_message .= ' (';
      if (isset($image_styles[$settings['slideshow_colorbox_image_style']])) {
        $link_type_message .= t('Image style: @style', array('@style' => $image_styles[$settings['slideshow_colorbox_image_style']]));
      }
      else $link_type_message .=  t('Original image');
      if (isset($settings['slideshow_colorbox_slideshow']) && $settings['slideshow_colorbox_slideshow']) {
        $colorbox_slideshow = array(
          'automatic' => t('Automatic'),
          'manual'    => t('Manual'),
        );
        $link_type_message .= ', with Slideshow (' . $colorbox_slideshow[$settings['slideshow_colorbox_slideshow']] . ' - Speed: ' . $settings['slideshow_colorbox_slideshow_speed'] . ')';
      }
      $link_type_message .= ')';
    }
    $summary[] = $link_type_message;
  }

  if ($field['type'] == 'media' || $field['type'] == 'field_collection')
    $caption_types = _field_slideshow_get_fields(array('text'), $field['type'], $field['field_name']);
  else $caption_types = array(
    'title' => t('Title text'),
    'alt'   => t('Alt text'),
  );
  // Display this setting only if there's a caption.
  if (isset($caption_types[$settings['slideshow_caption']])) {
    $caption_message = t('Caption: @caption', array('@caption' => $caption_types[$settings['slideshow_caption']]));
    if (isset($link_types[$settings['slideshow_caption_link']])) $caption_message .= ' (' . t('Link to: @link', array('@link' => $link_types[$settings['slideshow_caption_link']])) . ')';
    $summary[] = $caption_message;
  }

  $summary[] = t('Transition effect: @effect', array('@effect' => $settings['slideshow_fx']));
  $summary[] = t('Speed: @speed', array('@speed' => $settings['slideshow_speed']));
  $summary[] = t('Timeout: @timeout', array('@timeout' => $settings['slideshow_timeout']));
  $orders = array(
    'reverse' => t('Reverse order'),
    'random'  => t('Random order'),
  );
  if (isset($orders[$settings['slideshow_order']])) {
    $summary[] = $orders[$settings['slideshow_order']];
  }
  $pause_button_text = "";
  if (isset($settings['slideshow_controls_pause']) && $settings['slideshow_controls_pause']) $pause_button_text = " " . t("(with play/pause)");
  if (isset($settings['slideshow_controls']) && $settings['slideshow_controls']) $summary[] = t('Create prev/next controls') . $pause_button_text;
  if (isset($settings['slideshow_pause']) && $settings['slideshow_pause']) $summary[] = t('Pause on hover');
  if (isset($settings['slideshow_start_on_hover']) && $settings['slideshow_start_on_hover']) $summary[] = t('Activate on hover');

  switch ($settings['slideshow_pager']) {
    case 'number':
      $summary[] = t('Pager') . ': ' . t('Slide number');
    break;
    case 'image':
      $pager_image_message = t('Pager') . ': ' . t('Image') . ' (';
      if (isset($image_styles[$settings['slideshow_pager_image_style']])) {
        $pager_image_message .= t('Image style: @style', array('@style' => $image_styles[$settings['slideshow_pager_image_style']]));
      }
      else $pager_image_message .= t('Original image');
      $pager_image_message .= ')';
      $summary[] = $pager_image_message;
    break;
    case 'carousel':
      $pager_image_message = t('Pager') . ': ' . t('Carousel') . ' (';
      if (isset($image_styles[$settings['slideshow_carousel_image_style']])) {
        $pager_image_message .= t('Image style: @style', array('@style' => $image_styles[$settings['slideshow_carousel_image_style']]));
      }
      else $pager_image_message .= t('Original image');
      $pager_image_message .= ', ' . t('Number of visible images: @visible', array('@visible' => $settings['slideshow_carousel_visible']));
      $pager_image_message .= ', ' . t('Number of images to scroll by: @scroll', array('@scroll' => $settings['slideshow_carousel_scroll']));
      $pager_image_message .= ', ' . t('Transition speed: @speed', array('@speed' => $settings['slideshow_carousel_speed']));
      if ($settings['slideshow_carousel_skin']) $pager_image_message .= ', ' . t('Skin: @skin', array('@skin' => $settings['slideshow_carousel_skin']));
      if ($settings['slideshow_carousel_vertical']) $pager_image_message .= ', ' . t('Vertical');
      if ($settings['slideshow_carousel_circular']) $pager_image_message .= ', ' . t('Circular');
      if ($settings['slideshow_carousel_follow']) $pager_image_message .= ', ' . t('Follow slide');
      $pager_image_message .= ')';
      $summary[] = $pager_image_message;
    break;
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function cycle2_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'cycle2_default':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $settings['some_setting'] . $item['value']);
      }
      break;
  }

  return $element;
}


