<?php

function cycle2_get_options($options = array()) {
	static $cycle2_num = 0;
	$cycle2_num++;
	$slideshow_id = 'cycle2-slideshow-'. $cycle2_num;
	
	$default_options 																							= array();
	$default_options['cycle2'] 																		= array();
	$default_options['slideshow'] 																= array();
	
	$default_options['slideshow']['id'] 													= $slideshow_id;
	
	$default_options['cycle2']['data-cycle-fx'] 									= 'fade'; // The name of the slideshow transition to use.
	$default_options['cycle2']['data-cycle-easing'] 							= 'null'; // Name of the easing function to use for animations.
	$default_options['cycle2']['data-cycle-timeout'] 							= '2000'; // The time between slide transitions in milliseconds. 
	$default_options['cycle2']['data-cycle-speed'] 								= '500'; // The speed of the transition effect in milliseconds. 
	$default_options['cycle2']['data-cycle-slides'] 							= '> div'; // A selector string which identifies the elements within the slideshow container that should become slides. 
	$default_options['cycle2']['data-cycle-paused'] 							= 'false'; // If true the slideshow will begin in a paused state.
	$default_options['cycle2']['data-cycle-pause-on-hover'] 			= 'false'; // If true an auto-running slideshow will be paused while the mouse is over the slideshow. 
	$default_options['cycle2']['data-cycle-random'] 							= 'false'; // If true the order of the slides will be randomized. 
	$default_options['cycle2']['data-cycle-carousel-visible'] 		= '1'; 
	$default_options['cycle2']['data-cycle-carousel-vertical'] 		= 'false'; 
	$default_options['cycle2']['data-cycle-carousel-fluid'] 			= 'true'; 
	
	$default_options['cycle2']['data-cycle-overlay'] 							= '#'. $slideshow_id. ' '. '> .cycle-overlay';
	$default_options['cycle2']['data-cycle-overlay-template'] 		= '<div>{{title}}</div><div>{{desc}}</div>';
	$default_options['cycle2']['data-cycle-caption'] 							= '#'. $slideshow_id. ' '. '> .cycle-caption';
	$default_options['cycle2']['data-cycle-caption-template'] 		= '{{slideNum}} / {{slideCount}}';
	$default_options['cycle2']['data-cycle-pager'] 								= '#'. $slideshow_id. ' '. '> .cycle-pager';
	$default_options['cycle2']['data-cycle-pager-template'] 			= '<span>&bull;</span>';
	$default_options['cycle2']['data-cycle-prev'] 								= '#'. $slideshow_id. ' '. '> .cycle-prev';
	$default_options['cycle2']['data-cycle-next'] 								= '#'. $slideshow_id. ' '. '> .cycle-next';
	
	$default_options['cycle2'] = array_merge($default_options['cycle2'], isset($options['cycle2']) ? $options['cycle2'] : array());
	$default_options['slideshow'] = array_merge($default_options['slideshow'], isset($options['slideshow']) ? $options['slideshow'] : array());
		
	return $default_options;
}

function cycle2_examples_basic() {
	$variables = array();
	$slides1 = array(
		array(
			'src' => 'http://malsup.github.com/images/p1.jpg',
			'date' => 'May, 2012',
			'title' => 'Sonnenberg Gardens',
			'desc' => 'Sonnenberg Gardens',
		),
		array(
			'src' => 'http://malsup.github.com/images/p2.jpg',
			'date' => 'June, 2012',
			'title' => 'Redwoods',
			'desc' => 'Muir Woods National Monument',
		),
		array(
			'src' => 'http://malsup.github.com/images/p3.jpg',
			'date' => 'June, 2012',
			'title' => 'Angel ',
			'desc' => 'San Franscisco Bay',
		),
		array(
			'src' => 'http://malsup.github.com/images/p4.jpg',
			'date' => 'July, 2012',
			'title' => 'Raquette Lake',
			'desc' => 'Adirondack State Park',
		),
	);
	
	$slides2 = array(
		array(
			'src' => 'http://level1pc.com/wp-content/uploads/2012/01/ipad-header-main.jpg',
			'date' => 'May, 2012',
			'title' => 'Sonnenberg Gardens',
			'desc' => 'Sonnenberg Gardens',
		),
		array(
			'src' => 'http://www.thingsblogging.com/wp-content/uploads/2012/01/cropped-Humpback-Whale.jpg',
			'date' => 'June, 2012',
			'title' => 'Redwoods',
			'desc' => 'Muir Woods National Monument',
		),
		array(
			'src' => 'http://www.educloud.net.au/cloud/wp-content/uploads/2012/10/cloud-blue-icons.jpg',
			'date' => 'June, 2012',
			'title' => 'Angel ',
			'desc' => 'San Franscisco Bay',
		),
		array(
			'src' => 'http://d1xj6atg96ipjv.cloudfront.net/wp-content/uploads/2012/10/FoodShot4.jpg',
			'date' => 'July, 2012',
			'title' => 'Raquette Lake',
			'desc' => 'Adirondack State Park',
		),
		array(
			'src' => 'http://www.ipod-copy.net/wp-content/uploads/2011/10/ipod-pc.jpg',
			'date' => 'July, 2012',
			'title' => 'Raquette Lake',
			'desc' => 'Adirondack State Park',
		),
	);


	$slides3 = array(
		array(
			'src' => 'http://www.conceptclarity.co.uk/sites/default/files/avControl.png?1300814315',
			'date' => 'May, 2012',
			'title' => 'Sonnenberg Gardens ',
			'desc' => 'Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens Sonnenberg Gardens ',
		),
		array(
			'src' => 'http://ekindirect.com/media/promotionaliphonecases.gif',
			'date' => 'June, 2012',
			'title' => 'Redwoods',
			'desc' => 'Muir Woods National Monument',
		),
		array(
			'src' => 'http://www.easy-m.be/images/_extralarge/slide_imac_FR.jpg',
			'date' => 'June, 2012',
			'title' => 'Angel ',
			'desc' => 'San Franscisco Bay',
		),
		array(
			'src' => 'http://www.fjaderholmarnaskrog.se/images/bilder_bildspel/Krogen/Krogen%20index/Krogen-001.jpg',
			'date' => 'July, 2012',
			'title' => 'Raquette Lake',
			'desc' => 'Adirondack State Park',
		),
		array(
			'src' => 'http://www.egosketch.com/media/frontpage-slideshow/en/corporate.jpg',
			'date' => 'July, 2012',
			'title' => 'Raquette Lake',
			'desc' => 'Adirondack State Park',
		),
	);


	$options1['cycle2'] = array(
		'data-cycle-fx' => 'carousel',
		'data-cycle-timeout' => '100',
		'data-cycle-speed' => '100',
		'data-cycle-carousel-visible' => '2',
		'data-cycle-carousel-vertical' => 'false',
		'data-cycle-carousel-fluid' => 'true',
		'data-cycle-allow-wrap' => 'true',
		'data-cycle-paused' => 'true',
		'data-cycle-carousel-offset' => '240',
		//'data-cycle-starting-slide' => '3',  
		'data-cycle-swipe' => 'true',  
	);

	$options2['cycle2'] = array(
		'data-cycle-fx' => 'fade',
		'data-cycle-timeout' => '3000',
		'data-cycle-speed' => '3000',
		'data-cycle-paused' => 'false',
		'data-cycle-overlay-template'	=> '<div>{{title}}</div>',
	);
	
	$variables['slides'] = $slides2;
	$variables['options'] = cycle2_get_options($options2);
	return theme('cycle2_basic', $variables);
}